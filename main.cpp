#include <iostream>
#include <chrono>
#include <memory>

#include "scheduler/scheduler.hpp"
#include "scheduler/sample_tasks.hpp"

using namespace std;

bool getTask(TaskScheduler& scheduler);
void generateTask(TaskScheduler& scheduler, bool flag);

int main() {
    size_t threads_number = 0;

    cout << "Hello. This is simple multithread scheduler.\n"
            "Please enter number of threads: ";

    cin >> threads_number;

    TaskScheduler scheduler(threads_number);

    while (getTask(scheduler)) { };

    cout << "Scheduler going to finish";

    return EXIT_SUCCESS;
}

bool getTask(TaskScheduler& scheduler) {
    int key = 0;

    cout << "Chose one of next actions:\n"
         << "\t1) Create single task.\n"
         << "\t2) Create Mutlti tasks.\n"
         << "\t3) Create random generated tasks.\n"
         << "\telse) Stop program.\n"
         << "\tEOF) Stop program.\n";

    cin >> key;

    if (cin.eof()) {
        return false;
    }
    int task_number = 1;
    int is_random = true;
    if(1 == key) {
        is_random = false;
    } else if (2 == key) {
        is_random = false;
        cout << "Enter number of task to generate:\n";
        cin >> task_number;
    } else if (3 == key) {
        cout << "Enter number of task to generate:\n";
        cin >> task_number;
    } else {
        return false;
    }

    for(int i = 0; i < task_number; ++i) {
        generateTask(scheduler, is_random);
    }
    return true;
}

void generateTask(TaskScheduler& scheduler, bool is_random) {
    int type = 0;
    int Startdelay = 0;
    int executionTime = 0;
    bool repeatable = false;

    if (!is_random) {
        cout << "Enter task type:\n"
             << "\t1)CerrTask\n"
             << "\telse)CoutTask\n";
        cin >> type;

        cout << "Enter start delay(ms):\n";
        cin >> Startdelay;

        cout << "Enter execution time(s):\n";
        cin >> executionTime;

        cout << "should this task repeat:\n";
        cin >> repeatable;
    } else {
        type = (rand() % 2) + 1;
        Startdelay = rand() % 1000;
        executionTime = rand() % 5;
        repeatable = !(rand() % 11);
    }

    std::shared_ptr <Task> task;

    if(1 == type) {
        task = make_shared<CerrTask>(executionTime);
    } else {
        task = make_shared<CoutTask>(executionTime);
    }

    scheduler.addTask(task, Startdelay, repeatable);
}
