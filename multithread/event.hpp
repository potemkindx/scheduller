#pragma once

#include <condition_variable>
#include <mutex>
#include <cstdint>

class Event {
public:
    Event();
    ~Event();

    void Wait();

    int TimedWait(uint32_t ms);

    void Signal();

    void Broadcast();

private:
    std::mutex               mMutex;
    std::condition_variable  mEvent;
};
