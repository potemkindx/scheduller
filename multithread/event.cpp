#include "event.hpp"

#include <chrono>

Event::Event() {
}

Event::~Event() {
}

void Event::Wait() {
    std::unique_lock<std::mutex> lck(mMutex);
    mEvent.wait(lck);
}

int Event::TimedWait(uint32_t ms) {
    std::unique_lock<std::mutex> lck(mMutex);
    std::cv_status nret = mEvent.wait_for(lck, std::chrono::milliseconds(ms));

    return nret == std::cv_status::timeout;
}

void Event::Signal() {
    mEvent.notify_one();
}

void Event::Broadcast() {
    mEvent.notify_all();
}
