#pragma once

#include <memory>
#include <queue>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <future>
#include <mutex>

#include "event.hpp"
#include "task.hpp"

enum ExecutionStatus {
    STATUS_STOPPED = 0,
    STATUS_STARTING,
    STATUS_STOPPING,
    STATUS_RUNNING
};

//task controller
class TaskScheduler {
public:
    TaskScheduler(size_t threadsNumber = 1);
    ~TaskScheduler();

private:
    TaskScheduler(const TaskScheduler& other) = delete;
    TaskScheduler& operator = (const TaskScheduler& other) = delete;

public:
    void addTask(std::shared_ptr< Task > task, int delayMs = 0, bool repeatable = false);
    void stop();

protected:
    static void* schedulerPollTask(void* controller);

    struct TaskInfo {
        size_t                                             mTaskID;
        std::shared_ptr<Task>                              mTask;
        ExecutionStatus                                    mStatus;
        std::chrono::time_point<std::chrono::system_clock> mNextStartTime;
        int                                                mDelay;
        bool                                               mRepeatable;
    };

    std::shared_ptr<TaskInfo> getTask();

    ExecutionStatus getStatus();

    void setStatus(ExecutionStatus status);

    // wait for new task
    int wait();

    // wake-up pool for perform operations
    void wakeUp();

    size_t count();
private:
    //Custom comparator for priority queue
    struct TaskCompare {
        bool operator()(
            const std::shared_ptr <TaskInfo>& lhs,
            const std::shared_ptr <TaskInfo>& rhs
        ) {
            return lhs->mNextStartTime < rhs->mNextStartTime;
        }
    };

    struct ThreadContext {
    public:
        ThreadContext(TaskScheduler* pool);

        TaskScheduler*            mpPool;                   // pointer to thread pool instance
        std::thread               mThread;                  // thread ID
        std::promise< void >      mPromise;                 // FlagSignalization if thread started
        std::future < void >      mFuture;                  // Value retrived by promise
    };

    std::priority_queue<
        std::shared_ptr <TaskInfo>,
        std::vector <std::shared_ptr <TaskInfo> >,
        TaskCompare
    >                                               mTasks;          //set of enqued tasks
    std::mutex                                      mTaskQueueMutex;

    std::vector<ThreadContext>                      mThreadList;
    ExecutionStatus                                 mStatus;	    	// controller status flag
    Event                                           mEvent;			// synchronization event for EventPollTask() task
};
