#include "sample_tasks.hpp"
#include <iostream>
#include <thread>
#include <chrono>

#include <ctime>

using namespace std;

mutex CerrTask::mMtx;

CerrTask::CerrTask(int seconds) {
    sleep_duration = seconds;
}

CerrTask::~CerrTask() {
}

void CerrTask::execute() {
    auto start_time = chrono::system_clock::now();
    auto thread_id = std::this_thread::get_id();
    time_t start_t = std::chrono::system_clock::to_time_t(start_time);

    {
        lock_guard<mutex> lck(mMtx);
        cerr << "huge perfomance calculation processed by "
             << thread_id << endl;
    }

    this_thread::sleep_for(chrono::seconds(sleep_duration));

    {
        lock_guard<mutex> lck(mMtx);
        cerr << "huge perfomance calculation finished by "
             << thread_id << '\n'
             << "Execution info: start_time(" << ctime (&start_t)
             << ") duration in ms(" << std::chrono::duration_cast<std::chrono::milliseconds>(chrono::system_clock::now() - start_time).count()
             << ")" << endl;
    }
}

CoutTask::CoutTask(int sleep_useconds) {
    sleep_duration = sleep_useconds;
}

CoutTask::~CoutTask() {
}

void CoutTask::execute() {
    auto start_time = chrono::system_clock::now();
    time_t start_t = std::chrono::system_clock::to_time_t(start_time);

    auto thread_id = std::this_thread::get_id();

    cout << "simple calculation processed by "
         << thread_id << '\n';

    this_thread::sleep_for(chrono::seconds(sleep_duration));

    cerr << "simple calculation processed by "
         << thread_id << '\n'
         << "Execution info: start_time(" << ctime (&start_t)
         << ") duration in ms(" << std::chrono::duration_cast<std::chrono::milliseconds>(chrono::system_clock::now() - start_time).count()
         << ")" << endl;
}
