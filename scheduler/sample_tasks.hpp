#pragma once

#include "task.hpp"
#include <mutex>

class CerrTask: public Task {
public:
    CerrTask(int sleep_seconds = 2);
    virtual ~CerrTask();
public:
    virtual void execute();
private:
    static std::mutex mMtx;
    int sleep_duration;
};

class CoutTask: public Task {
public:
    CoutTask(int sleep_useconds = 0);
    virtual ~CoutTask();
public:
    virtual void execute();
private:
    int sleep_duration;
};
