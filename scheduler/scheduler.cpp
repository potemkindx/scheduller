﻿#include "scheduler.hpp"
#include <atomic>

using namespace std::chrono_literals;
using namespace std::chrono;
using std::shared_ptr;
using std::thread;
using std::mutex;

TaskScheduler::TaskScheduler(size_t threadsNumber) {
      mStatus = STATUS_STARTING;

      mThreadList.reserve(threadsNumber);

      for (size_t i = 0; i < threadsNumber; ++i) {
          mThreadList.emplace_back(this);
      }

      mStatus = STATUS_RUNNING;
      for(auto& ctx: mThreadList) {
          ctx.mFuture.wait();
      }
}

TaskScheduler::~TaskScheduler() {
    stop();
}

void TaskScheduler::addTask(shared_ptr<Task> task, int delayMs, bool repeatable) {
    static std::atomic_size_t task_index(0);

    shared_ptr<TaskInfo> info (new TaskInfo);

    info->mTaskID = ++task_index;
    info->mTask = task;
    info->mDelay = delayMs;
    info->mNextStartTime = system_clock::now() + milliseconds(delayMs);
    info->mRepeatable = repeatable;
    info->mStatus = STATUS_STOPPED;

    mTaskQueueMutex.lock();
    mTasks.push(info);
    mTaskQueueMutex.unlock();

    mEvent.Broadcast();
}

void TaskScheduler::stop() {
    if(getStatus() != STATUS_RUNNING) {
       return;
    }

    // set termination flag
    setStatus(STATUS_STOPPING);
    // immediately wake-up PollAll() task if it's in wait state
    mEvent.Broadcast();
    // give PollTask() task chance for finalize gracefully
    std::this_thread::sleep_for(2s);

    mTaskQueueMutex.lock();
    while(!mTasks.empty()) {
        mTasks.pop();
    }
    mTaskQueueMutex.unlock();

    // wait a bit for thread
    // try to correctly finalize threads
    for(ThreadContext& ctx : mThreadList) {
        // wake-up threads again
        mEvent.Broadcast();

        // here should be timedjoin but i dunno how to implement it with stl
        seconds ts(1);
        ctx.mThread.join();
    }

    setStatus(STATUS_STOPPED);
}

void *TaskScheduler::schedulerPollTask(void *ptr) {

    ThreadContext* ctx = static_cast<ThreadContext*>(ptr);

    ctx->mPromise.set_value();

    while(true) {
        // wait for broadcast message which indicates that work queue is not empty
        ctx->mpPool->wait();
        // exit if flag set
        if(ctx->mpPool->getStatus() != STATUS_RUNNING &&
           ctx->mpPool->getStatus() != STATUS_STARTING) {
            break;
        }

        // execute task
        shared_ptr<TaskInfo> task = ctx->mpPool->getTask();

        if(task == nullptr) {
            continue;
        }

        auto current_moment = system_clock::now();

        std::this_thread::sleep_for(task->mNextStartTime - current_moment);

        task->mTask->execute();

        if(task->mRepeatable) {
            std::lock_guard<mutex>(ctx->mpPool->mTaskQueueMutex);
            task->mNextStartTime
                = system_clock::now()
                + milliseconds(task->mDelay);

            ctx->mpPool->mTasks.push(task);
        }

        // work queue is not empty, send signal to threads to wake-up
        if(ctx->mpPool->count()) {
            ctx->mpPool->wakeUp();
        }
    }

    return nullptr;
}

shared_ptr<TaskScheduler::TaskInfo> TaskScheduler::getTask() {
    shared_ptr<TaskInfo> task(nullptr);

    mTaskQueueMutex.lock();
    if(!mTasks.empty()) {
        task = mTasks.top();
        mTasks.pop();
    }
    mTaskQueueMutex.unlock();

    return task;
}

ExecutionStatus TaskScheduler::getStatus() {
    return mStatus;
}

void TaskScheduler::setStatus(ExecutionStatus status) {
    mStatus = status;
}

int TaskScheduler::wait() {
    return mEvent.TimedWait(100); // 100 ms
}

void TaskScheduler::wakeUp() {
    mEvent.Broadcast();
}

size_t TaskScheduler::count() {
    return mTasks.size();
}


TaskScheduler::ThreadContext::ThreadContext(TaskScheduler *pool)
    : mpPool(pool), mThread(TaskScheduler::schedulerPollTask, this),
      mPromise(), mFuture(mPromise.get_future())
{ }
