#pragma once

//scheduler task interface
class Task {
public:
    Task();
    virtual ~Task();
public:
    virtual void execute() = 0;
//  virtual void abort() = 0;
};
